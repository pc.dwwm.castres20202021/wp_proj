<?php
//fonction permettant l'ajout du style sur tout les pages
// function_exists vérifie si la fonction est valides si elle est valide sa l'éxécute 
if( ! function_exists( 'my_scripts' ) ):
function my_scripts() {
	 // vérifie si la ressource css et disponible en allant chercher le get_style comme pour le header/footer
	 // et la met en file d'attente 
	wp_enqueue_style( 'style', get_stylesheet_uri());
}
endif;

	// le add_action vas déclencher le script wp qu'il y a en file d'attente,
	// en l'occurance a ce moment précis le hook de wp
	// lancera le script "my_scripts" qui contient le style de la page.
add_action( 'wp_enqueue_scripts', 'my_scripts');


//fonction triant mes posts par ordre alphabétique

 function foo_modify_query_order( $query ) {
	// si la varible $query est que cette même variable et définie sur la même page 
	// et que c'est la page ou la fonction est apliqué alors on définie les changements 

 	if ( $query->is_home() && $query->is_main_query() ) {

 		$query->set( 'orderby', 'title' );

 		$query->set( 'order', 'ASC' );
 	}

 }

 	// hook qui déclanche la fonction de trie 
	// le pre_get_posts est un hook qui trés largement utilisé et qui vas d'abbord récupérer le post et ensuite 
	// appliquer la fonction définie

 add_action( 'pre_get_posts', 'foo_modify_query_order' );  



//Ajoute une notice sur le dashboard pour installer ACF


// le hook general_admin_notice permet d'afficher des messages a l'admin 
// la variable globale $pagenow signifie que le message s'appliquera sur la page en cours d'affichage
// et si celle-ci est la page index.php alors ont affiche l'echo.
 function general_admin_notice(){
    global $pagenow;
    if ( $pagenow == 'index.php' ) {

         echo '<div class="notice notice-warning is-dismissible">
             <p>You must add ACF plugin to your wordpress.</p>
         </div>';
    }
}

// hook permettant de lancer l'admin notice qui vas afficher le message et l'affiche dans les messages généraux de l'admin
add_action('admin_notices', 'general_admin_notice'); 



//Code php générer pour les field groups à intégrer pour l'export du thème
//bout de code générer sur acf dans: 
// --> acf --> tool --> export sous forme de php 

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_60291fd7baed9',
		'title' => 'compositeur',
		'fields' => array(
			array(
				'key' => 'field_60291ffe4aad5',
				'label' => 'Enluminure',
				'name' => 'enluminure',
				'type' => 'image',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
			array(
				'key' => 'field_602921af4aad6',
				'label' => 'lien Oeuvre',
				'name' => 'lien_oeuvre',
				'type' => 'url',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
			),
			array(
				'key' => 'field_6029220b4aad7',
				'label' => 'mange',
				'name' => 'mange',
				'type' => 'relationship',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array(
					0 => 'post',
				),
				'taxonomy' => '',
				'filters' => array(
					0 => 'search',
					1 => 'post_type',
					2 => 'taxonomy',
				),
				'elements' => '',
				'min' => '',
				'max' => '',
				'return_format' => 'object',
			),
			array(
				'key' => 'field_602953934f2d6',
				'label' => 'nom oeuvre',
				'name' => 'nom_oeuvre',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;
?>