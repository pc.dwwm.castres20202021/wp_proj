
<!-- le get header & footer marche pareil que les includes en php -->
<?php get_header(); ?>

	<div class="container">
		<h1 class="mainTitle">Index</h1>
		<div class="alpha">

			<!-- boucle qui vas récupérer les postes: 
			tant qu'il y a des posts qui sont créé, alors on les affiche -->
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<!-- créé le liens qui permet d'aller sur les différents articles c'est liens sont mis sur les titres récupéré via les post -->
				<a href="<?php the_permalink(); ?>" class="lien">
					<?php the_title('<h2>', '</h2>'); ?>
				</a>
			<!-- le endwhile ferme la boucle de récupération et le else affiche une erreur si aucun poste n'est transmit, 
			le _e est compris pour wordpress comme traduit moi ce qu'il y a dans les premières côtes le texte a écrire et dans les autres côtes c'est le chemin par défaut de où trouver le ce texte -->
			  <?php	endwhile; else :_e( 'Sorry, no posts matched your criteria.', 'textdomain' ); endif; ?> 
			
		</div>
	</div>
<?php get_footer(); ?>