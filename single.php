<!-- fonction qui vas chercher l'autre fonction wp_head() qui est dans le fichier header.php  -->

<?php get_header(); ?>

	<div class="container">
					<!-- fonction qui récupére le titre des post et l'affiche -->
	<h1 class="mainTitle"><?php the_title(); ?></h1>
					<!-- fonction qui récupére l'image du champ "enluminure" et ajoute le titre dans la déscription -->
	<img class="imgcomp" src="<?php echo get_field('enluminure'); ?>" alt="<?php the_title(); ?>">
		<!-- the content est la fonction qui permet d'afficher le contenue -->
		<?php the_content(); ?>
			<h3>Oeuvre exemple:</h3>
			<ul>
			<!-- fonction qui récupére le lien dans le champs et ajoute le liens au nom de l'ouvre 
			afin que celle-ci renvois vers la page de l'animal sélectionné -->
				<a href="<?php echo get_field('lien_oeuvre'); ?>"><li><?php echo get_field('nom_oeuvre'); ?></li></a>
			</ul>
		</p>
		<p>
			<h3>Mange:</h3>

			<!-- affiche la variable mange qui est récupéré dans le champ mange -->
			<?php $mange= get_field('mange'); 
			//var_dump($mange);?>
			<ul>
			<!-- boucle: pour chaque aliment de cet animal, ont ajoute le lien vers celui-ci sur le nom de celui-ci -->
				<?php foreach ($mange as $compositeur) { ?>
					<a href="<?php echo $compositeur->guid; ?>">
						<li><?php echo $compositeur->post_title; ?></li>
					</a>
				<?php } ?>
				
				
			</ul>
		</p>
	
</div>
<?php get_footer(); ?>
<!-- fonction qui vas chercher l'autre fonction wp_footer() qui est dans le fichier header.php  -->


